% Created 2021-06-04 vie 23:03
% Intended LaTeX compiler: pdflatex
\documentclass[14pt, spanish]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{bigfoot}
\DeclareNewFootnote{URL}[roman] % href footnotes
\renewcommand{\href}[2]{#2\footnoteURL{\url{#1}}}
\hypersetup{linktoc = all, colorlinks = true, urlcolor = DodgerBlue4, citecolor = PaleGreen1, linkcolor = black, hypertexnames=true}
\usepackage[x11names]{xcolor}
\hypersetup{allcolors=blue,}
\author{David Pineda Osorio}
\date{\today}
\title{Dos formas de crear presentaciones con Org-mode}
\hypersetup{
 pdfauthor={David Pineda Osorio},
 pdftitle={Dos formas de crear presentaciones con Org-mode},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.4.3)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle
\tableofcontents


\section{Introducción}
\label{sec:org0591a36}

Hasta el momento hemos conocido el uso del editor \textbf{Emacs} y la producción de
\emph{documentos} de \textbf{Org-mode} con sus principales elementos. 

La \emph{composición} adecuada  de uno de los elementos nos permiten mostrar o
informar de manera efectiva.

La creación de documentos podrá tener diferentes destinos o \emph{público objetivo} o
\emph{momento específico} en que su contenido será comunicado. Por lo que será
necesario escoger adecuadamente la mejor presentación para tal objetivo.

Dado que \textbf{Org-mode} puede operar como el \emph{documento semilla} de diversos
\emph{destinos}, en esta sesión he preparado dos formas de crear \emph{presentaciones de
diapositiva}, comumente llamanadas en estos tiempos \emph{posponte}. Podemos
considerar estos también como un ejercicio práctico para ir dejando de lado un
uso de \emph{marca registrada y privativa} a un nombre más significativo.

Las dos formas de crear presentaciones usan diferentes técnicas de creación.
Nuestro sistema de edición \textbf{Org-mode} operará como intermediario base, lo que
nos facilitará el trabajo y nos permitirá evitar dedicar tiempo a una u otra
adicionalmente.

Cada técnica diferente necesitará realizar algunas configuraciones para poder
operar en el flujo de trabajo de \textbf{Org-mode}. Para eso veremos de qué trata cada
una, sus posibilidades y limitaciones.

\section{Presentaciones con Beamer}
\label{sec:org28ad257}

El sistema de presentaciones \textbf{Beamer} es un \emph{módulo de latex}, por lo que
permitiría hacer todo lo que \emph{latex} permite en principio. 

Si aún no sabes muy bien que es \emph{latex} en corto te puedo decir que es todo un
conjunto de herramientas que, mediante marcas (tal como org-mode pero más
complejas), permite crear documentos con una presentación de calidad
excepcional.

Con \emph{latex} se hace sencillo trabajar con \emph{fórmulas}, \emph{tablas} y otros
componentes de textos para ciencias, humanidades y literatura. Por lo que todo
lo que ofrece \emph{latex} es trasladable a \emph{Beamer}.

Entonces, desde \textbf{Org-mode} tendremos que seguir una \emph{estructura} del texto y sus
elementos para que pueda ser \emph{exportado} adecuadamente a \emph{Beamer}.

El \href{https://osl.ugr.es/CTAN/macros/latex/contrib/beamer/doc/beameruserguide.pdf}{Documento Manual} te permitirá adentrarte en los detalles de lo que ofrece
\emph{Beamer}. Lo principal para nosotros es lo siguiente:

\begin{itemize}
\item Configurar la salida a \emph{Beamer}
\item Escoger el tema o presentación
\item Crear el contenido para presentación
\end{itemize}

Para trabajar con presentaciones te recomiendo activar \textbf{indent-mode} para que
puedas controlar visualmente a que correspondería cada diapositiva.

\begin{verbatim}
M+x org-indent-mode
\end{verbatim}

\subsection{Configurar \emph{Beamer} para spacemacs}
\label{sec:org30a585f}

Siguiendo el tutorial de la \href{https://orgmode.org/worg/exporters/beamer/tutorial.html}{Documentación Org-mode}, podemos configurar
adecuadamente nuestro sistema para creara presentaciones \emph{Beamer}. Este sistema
trabaja con archivos \textbf{pdf} como formato de archivo final.

Cada \emph{diapositiva}, \emph{slide}, o \emph{frame} como se le conoce en \emph{Beamer} será 1
página del archivo a producir.

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/beamer_1pag.png}
\caption{\label{ejemplobeamer}Primera página de diapositiva en Beamer}
\end{figure}

\subsubsection{Instalación}
\label{sec:org9327bb4}

Necesitarás, al menos tener instalado el paquete \emph{latex Beamer}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
sudo apt install python-pygments
sudo apt install python3-pygments
sudo apt install texlive-latex-recommended
sudo apt install texlive-latex-extra
sudo apt install texlive-fonts-recommended ttf-marvosym
\end{minted}

\subsection{Creación de presentaciones para Beamer}
\label{sec:orgba443b8}

\subsubsection{En el documento orgmode}
\label{sec:org6eb0c2a}


Al encabezado usual de \textbf{Org-mode} con destino \emph{latex}, bastaría con añadir como
base el siguiente trozo.

\begin{verbatim}
#+startup: beamer
#+STARTUP: indent
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
\end{verbatim}

Ahora, se añade la etiqueta que define en que nivel los títulos se consideran
una sección o \emph{nueva diapositiva}.

\begin{verbatim}
#+OPTIONS:   H:2 num:t toc:t 
#+BEAMER_FRAME_LEVEL: 2
\end{verbatim}

Para definir los tamaños de cada parte de la diapositiva.

\begin{verbatim}
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)
\end{verbatim}

La selección de temas, de entre los disponibles:

\begin{verbatim}
#+BEAMER_THEME: Rochester [height=20pt]
\end{verbatim}

Si deseas seleccionar una fuente específica.

\begin{verbatim}
#+BEAMER_FONT_THEME: 
\end{verbatim}

También, si te dedicas a crear algo especial, lo puedes usar. Sin embargo, para
la mayoría de los casos los temas disponibles son suficientes.

Además del \emph{título}, si deseas poner un \emph{subtítulo}.


\begin{verbatim}
#+TITLE: La vegetación de clima mediterráneo
#+SUBTITLE: Reguladores de ecosistemas
\end{verbatim}


Si deseas clasificar con algunas \emph{etiquetas} la presentación y \emph{describir}. Esta
información se asociará a la \emph{metadata}.

\begin{verbatim}
#+DESCRIPTION: Es la presentación del trabajo de tesis sobre el papel regulador
de la vegetación en ecosistemas de clima mediterráneo
#+KEYWORDS: vegetación ecosistema
\end{verbatim}

Para escoger, de entre las opciones, puedes visitar la \href{https://deic-web.uab.cat/\~iblanes/beamer\_gallery/index\_by\_theme.html}{galería}. Tiene una vista
general y puedes ver el detalle.

Cada elemento, además, puede llevar definido atributos específicos de \emph{Beamer},
añadiendo sus propiedades a la etiqueta \textbf{ATTR\_BEAMER}.

\begin{verbatim}
#+ATTR_BEAMER: :environment nonindentlist
- item 1, not indented
- item 2, not indented
- item 3, not indented
\end{verbatim}

Estos atributos son (según la configuración Beamer):

\begin{description}
\item[{:environment}] cambia configuración de manera local
\item[{:overlay}] para cubrir, usando "<>" i "[]"
\item[{:options}] para insertar argumentos opcionales
\end{description}

\subsubsection{Marcos y Bloques en Beamer usando Org-mode}
\label{sec:org87bac13}

Cuando necesitemos diapositivas que dispongan de más de una columna para mostrar
información, como una lista y una imagen, o demarcar una diapositiva de manera
diferente, es posible definir tipos de marcos y bloques desde \textbf{Org-mode}, aprovechando
una de las características de \emph{Beamer}.

Al definir (setear) la propiedad \textbf{BEAMER\_ENV} en los valores \textbf{frame} o \textbf{fullframe}. Org
convierte en \emph{marcos} aquellas secciones que se correspondan al nivel de \textbf{H}
dado en \textbf{OPTIONS}. Si el valor se define en \textbf{appendix} Org la exporta como
apéndice. Si se define como \textbf{note}, se exporta como nota.

Valores de \textbf{BEAMER\_ENV}

\begin{description}
\item[{frame}] la sección ocupa el frame completo
\item[{fullframe}] lo mismo que frame
\item[{ignoreheading}] se ignora que exista el título
\item[{example}] ejemplo
\item[{appendix}] apéndice
\item[{node}] nota
\item[{theorem}] teorema
\item[{block}] bloque
\item[{definition}] definición
\end{description}

Los \emph{bloques} y \emph{marcos} permiten definir recuadros y estilos de la diapositiva.

Este texto define una diapositiva con un bloque \textbf{teorema}, ejemplo de bloque
especial \ref{bloque}

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/bloque_teorema.png}
\caption{\label{bloque}Diapositiva con bloque \textbf{teorema}}
\end{figure}


Para tales efectos debemos activar la \textbf{sección} o \textbf{subsección} con las
propiedades especiales.

Luego del título, en las líneas siguientes definir las propiedades del bloque
especial. 

\begin{verbatim}
:PROPERTIES:
:BEAMER_ENV: theorem
:END:
\end{verbatim}

De manera similar, para crear una diapositiva de \textbf{2 columnas}. Inmediatamente
bajo el título de la \textbf{subsección}, se debería añadir la definición del \textbf{ancho de
columna}, que debe ser un valor de 0 a 1 proporcional al ancho.

\begin{verbatim}
:PROPERTIES:
:BEAMER_COL: 0.45
:END:
\end{verbatim}

También, es posible definir los parámetros de \emph{BEAMER}, utilizando la etiqueta,
dentro de las \textbf{PROPERTIES}. Se puede usar \textbf{[t]}, \textbf{[<+->]}, \textbf{<2->}. Si se define un ancho
de columnas, se pueden definir la orientación de cada una de ellas. 

\begin{verbatim}
:BEAMER_envargs: [t]
\end{verbatim}

\begin{description}
\item[{[t]}] alineación vertical hacia arriba de los bloques.
\end{description}

\subsubsection{Ejemplos y guías.}
\label{sec:orge79d9f9}

Los siguientes enlaces son algunos de los sitios que he encontrado ejemplos y
usos de \textbf{org-mode} con \emph{Beamer}.

\begin{description}
\item[{ehneilsen.net}] \url{http://ehneilsen.net/notebook/orgExamples/org-examples.html}
\item[{tty.cl}] \url{https://tty.cl/org-mode-and-beamer.html}
\item[{ref-cardas en beamer}] \url{https://github.com/fniessen/refcard-org-beamer}
\item[{tutorial}] \url{https://orgmode.org/worg/exporters/beamer/ox-beamer.html}
\item[{youtube 1}] \url{https://www.youtube.com/watch?v=vz9aLmxYJB0}
\item[{youtube 2}] \url{https://www.youtube.com/watch?v=cTrizroQeL8}
\item[{curso de beamer}] \url{https://www.uv.es/\~famarmu/doc/beamer-adv-uji-slides.pdf}
\item[{themes extras}] \url{https://es.overleaf.com/gallery/tagged/presentation}
\item[{matriz de themes}] \url{https://hartwork.org/beamer-theme-matrix/}
\end{description}

\section{Presentaciones con Reveal.js}
\label{sec:org95f6b91}

Este sistema de presentaciones se basa en la tecnología \textbf{html} y el \emph{framework}
llamado \textbf{Reveal.js} que, en base una estructura del código \emph{html} crea una
presentación de diapositivas. De manera muy parecida a como lo hace \emph{Beamer}.

Visitemos la \href{https://revealjs.com/}{web} para ver que nos ofrece. 

\begin{itemize}
\item Enlaces
\item Visualización dinámica
\item Fórmulas
\item Temas
\item Etcétera
\end{itemize}

En términos prácticos, no ofrece lo mismo que \emph{Beamer}, con otra tecnología y
estilos.

En general, la edición de los estilos o temas visuales puede llegar a ser más
fácil con \textbf{Reveal} ya que es más conocido o fácil de usar la definición de
estilos por cascada \textbf{css}.

\subsection{Configurar Reveal.js para org-mode}
\label{sec:org1de05c7}

Para activar la exportación a presentaciones hechas con \textbf{Reveal} será necesario
descargar (o clonar) el paquete \textbf{org-reveal}


También el código base que consiste en el \textbf{framework Reveal}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
mkdir ~/software
cd ~/software
wget https://github.com/hakimel/reveal.js/archive/master.zip
unzip -qq master.zip 
mv reveal.js-master reveal.js
\end{minted}


En el bloque de \textbf{layers} de \textbf{\textasciitilde{}/.comicios} editar la línea de \textbf{org}, o bien añadir
a lo que ya tengas:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{common-lisp}
(org :variables
     org-enable-reveal-js-support t
     )
\end{minted}

Luego, en el bloque \textbf{user-config}, decirle a \textbf{Emacs} donde tendrá que buscar el
\textbf{framework reveal} en cada proyecto.

En mi caso lo he puesto así (puede ser una ruta relativa o específica), para un
proyecto de un curso de python.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{common-lisp}
(setq org-reveal-root "../")
\end{minted}

Por ejemplo, como se ve la imagen \ref{revealpath}
\begin{figure}[htbp]
\centering
\includegraphics[width=350px]{./img/path_reveal.png}
\caption{\label{revealpath}Ubicación del paquete reveal.js}
\end{figure}


Si piensas utilizar de manera permanente \textbf{reveal}, puedes dar una ruta como la
que se configura a continuación.

De todas maneras, una vez que lo hagas \textbf{org-mode} detectará que existe la
posibilidad de que vayas a exportar a \textbf{Reveal} que es \textbf{html+css+js} y te lo
mostrará en el menú\ref{revealmenu}. Si no te construye el \textbf{html} con \textbf{Reveal} es
porque has dado una ruta incorrecta.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{common-lisp}
(setq org-reveal-root "/home/USUARIO/software/reveal.js")
\end{minted}

\begin{figure}[htbp]
\centering
\includegraphics[width=350px]{./img/reveal_menu.png}
\caption{\label{revealmenu}Menú de exportación a reveal.js}
\end{figure}

\subsection{Estructura de una presentación reveal.}
\label{sec:org21c7fbb}

Necesitarás declarar las isguientes etiquetas.

Entregar la ruta o el \textbf{path}, si no lo has hecho ya por defecto en la
configuración global.

\begin{verbatim}
#+REVEAL_ROOT: ../reveal.js
\end{verbatim}

Seleccionar \emph{theme} base, de entre los disponibles, puedes ver \href{https://revealjs.com/themes/}{acá}.

\begin{verbatim}
#+REVEAL_THEME: league
\end{verbatim}

Definir las transisiones entre diapositivas, seleccionar \href{https://revealjs.com/transitions/}{acá}.

\begin{verbatim}
#+REVEAL_TRANS: linear
\end{verbatim}

Inicialización, tamaño por defecto, páginas, etc.

En js se tiene algo así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{javascript}
Reveal.initialize({
  width: 960,
  height: 700,
  margin: 0.04,
  minScale: 0.2,
  maxScale: 2.0
});
\end{minted}

En orgmode:

\begin{verbatim}
#+REVEAL_INIT_OPTIONS: slideNumber:true,width:1366,height:768, center:false
\end{verbatim}

Estilos especiales.

\begin{verbatim}
#+REVEAL_EXTRA_CSS: ../css/estilos.css
\end{verbatim}

Opciones de niveles de diapositivas. Tal como con \emph{Beamer}, \textbf{H:2} nos permite
indicar que las secciones de primer nivel son 'capítulos' y las de segundo nivel
serán diapositivas que \emph{integran} cada capítulo. 

\begin{verbatim}
#+OPTIONS: H:2 toc:1 num:f author:t date:f email:t
\end{verbatim}

Un ejemplo de estilos que se pueden definir:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{css}
body:after {
content: url(../img/logo.png);
position: fixed;
bottom: 2em;
left: 2em;
box-shadow: 3px 3px 6px #000; }
.reveal .slides > section > section > p { text-align:left; }
\end{minted}

\subsection{Creación de presentaciones para Reveal.js}
\label{sec:orgbceef3e}

Para crear las presentaciones basta con escribir de manera normal, incluyendo
los elementos que necesitas, bajo un tamaño adecuado y correspondiente al tamaño
de la diapositiva.

\subsection{Referencias}
\label{sec:org80c2261}

\begin{description}
\item[{Website}] \url{https://revealjs.com}
\item[{Curso de reveal}] \url{https://revealjs.com/course/}
\item[{Youtube}] \url{https://www.youtube.com/watch?v=fTcdxXnSWso}
\item[{Youtube 2}] \url{https://www.youtube.com/watch?v=psDpCpcIVYs}
\end{description}

\section{Ejercicio.}
\label{sec:org1b695de}

Este ejercicio consiste en seleccionar un modo de presentaciones, y crear una
con diferentes tipos de elementos para visualizar.

Piensa que podría ser:

\begin{itemize}
\item Películas y directores
\item Libros hechos películas
\item Cuentos y novelas de ciencia ficción
\item Lenguajes de programación
\item Música, artistas y sus instrumentos
\item Ciencia y el aporte a la humanidad
\item Transportes
\item Pandemias en la historia humana.
\item etc.
\end{itemize}
\end{document}
