(TeX-add-style-hook
 "1_tipos_datos"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem") ("babel" "spanish")))
   (add-to-list 'LaTeX-verbatim-environments-local "minted")
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref"
    "minted"
    "babel")
   (LaTeX-add-labels
    "sec:org03b0445"
    "sec:org801b1fd"
    "sec:org37cb0a0"
    "sec:orgf4c0ad2"
    "sec:orga1e0d82"
    "sec:org5ba05de"
    "sec:org95a8147"
    "sec:orgffcc449"
    "sec:org73389c0"
    "sec:orgf8d3177"
    "sec:orgb9d8f37"
    "sec:org2ac170c"
    "sec:org21a94ca"
    "sec:org53c7e96"
    "sec:org02a2658"
    "sec:org1f4aadd"
    "sec:org162870a"
    "sec:orgeb2edb4"
    "sec:org7cfa092"
    "sec:org03772d7"
    "sec:org2cacb90"
    "sec:orgfdd91e0"
    "sec:orgedec006"
    "sec:org25c0b3e"
    "sec:org0692893"
    "sec:org82ecdbf"
    "sec:orgfdf3e73"
    "sec:orgecc4437"
    "sec:org4495082"
    "sec:orge6f6620"
    "sec:org157bf55"
    "sec:org1d7690d"
    "sec:org31cff5a"
    "sec:orgefa9783"
    "sec:orgeba315c"
    "sec:org798f4ab"
    "sec:org756d7ee"
    "sec:org49a8908"
    "sec:org7dc0a63"
    "sec:orgf18d9f2"
    "sec:org7fd513e"
    "sec:orgc8dded3"
    "sec:orgd52c5d8"
    "sec:orga24a7b3"
    "sec:org42bd31b"
    "sec:org5ba9a86"
    "sec:org5ff039b"
    "sec:orgaf4d2b8"
    "sec:org8ceddca"
    "sec:org115f68c"
    "sec:orgc141d38"
    "sec:org650af27"
    "sec:org4f92dc9"
    "sec:org4eb7eb3"
    "sec:orgfa3df76"
    "sec:org25a46a5"
    "sec:orge7eeeee"
    "sec:orgf643e4c"
    "sec:org3140820"
    "sec:org04a8c5c"
    "sec:org90655c6"
    "sec:orgf4ac2ea"
    "sec:orgdd7b85d"
    "sec:orgeb4a8a4"
    "sec:orgffd5015"
    "sec:org35a20ea"))
 :latex)

