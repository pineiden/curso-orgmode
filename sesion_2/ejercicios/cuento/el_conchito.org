#+TITLE: Los retoños.
#+AUTHOR: David Pineda
#+OPTIONS: toc:nil
#+latex_class_options: [20pt]

Desde un rincón del patio estaba la gallina /colorada/, la más viejita del
gallinero tomándose un baño de polvo y esperando la tarde para subir a una de
las ramas y dormir al lado del resto de las gallinas.

Ya estaba viejita, había vivido cerca de diez años, que son como ciento
cincuenta en edades de gallina. Así que de colorada le quedaba poco, se movía
lento y ya no corría a comer las semillitas que encontraba el gallo por la
mañana. 

Pese a lo viejita aún ponía huevos y soñó todo el verano con empollar un grupito
de nuevos pollitos. Sin embargo, el ama ya no le dejaba nada cuando entraba en
calentura. La última vez sus tres pollitos no le duraron mucho, así que ya la
daban por descartada.

El resto de las gallinas, la blanquita que era la más jóven, la pintada que le
gustaba irse por la acequia picando apios, la negrita que era una golosa de los
gusanitos y las otras, estaban conversando bajito a lo lejos. La
querían mucho a la viejita, era madre de la    
pintada y a las otras seguro que las había críado. No se acordaban demasiado, pero
estaban seguras que siempre estaba por ahí enseñándoles a picotear los pastos,
encontrar las semillas más gorditas y así con el pasar del tiempo se convirtió
en la última.

-- Le haremos un regalo a la /colorada/ -- Dijo la blanquita

-- ¿Qué podrá ser? -- preguntó el gallo que andaba por ahí escuchando.

-- Bueno, siempre cuando salimos a dar vueltas me cuenta de sus pollitos, que le
   gustaría volver a vivir la crianza. -- cuenta la pintada.

-- ¡Ah, La tengo!

-- A ver, ¡cuenta!

-- Pues cada una de nosotras le regalaremos un huevo que pongamos. Le haremos un
   nido bien escodido y la llevaremos a que los empolle.

Y así sucedió. Para el día de su cumpleaños, que era a fines de marzo, las
gallinas habían preparado un nido bien escondido entre los matorrales, con
pajitas y palitos para proteger los huevos de animales y del viento.  

-- ¡/Colorada/, viejita, ven que te tenemos un regalo! -- cantaron todas en su
   lengua de gallinas.

Ella las siguió, todas la acompañaron, el gallo bien de atras, curioso por ver
que pasaba. La viejita colorada se sorprendió, creía que nadie se acordaba de
ella y terminaría por perderse de vieja entre los campos. Pero no, ahí estaban
sus hijitas llevándole a un hermoso regalo.

Se quedó ahí, cada día las gallínas más jóvenes iban y le hacían compañia en el
nido, eran muuuchos huevos y la /colorada/ estaba chiquitita. Luego de tres
semanas poquito a poquito fueron apareciendo desde dentro de cada huevito
pequeños pollitos de todos los colores, salieron piándo, pidiendo comida y
agüita, hambrientos estaban.

Cuando ya nació el último, decidieron salir al patio. Fué todo un espectáculo,
la luz del sol estaba brillante y había un suave viento fresco, era una madre
radiante. El ama creía que ya no estaba en este mundo, pero ¡sí! Ahí estaba
ella, con sus nuevos hijitos. 

Y así fue como la viejita /colorada/ tuvo sus últimos retoños, los últimos del
gallinero en esa temporada. Todas las gallinas más jóvenes la ayudaron a
criarlos y la pudieron ver feliz en sus últimos días de vida. 
