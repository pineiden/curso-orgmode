% Created 2021-05-28 vie 21:59
% Intended LaTeX compiler: pdflatex
\documentclass[14pt, spanish]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{bigfoot}
\DeclareNewFootnote{URL}[roman] % href footnotes
\renewcommand{\href}[2]{#2\footnoteURL{\url{#1}}}
\hypersetup{linktoc = all, colorlinks = true, urlcolor = DodgerBlue4, citecolor = PaleGreen1, linkcolor = black, hypertexnames=true}
\usepackage[x11names]{xcolor}
\hypersetup{allcolors=blue,}
\author{David Pineda}
\date{\today}
\title{Documentos con diversidad de contenidos}
\hypersetup{
 pdfauthor={David Pineda},
 pdftitle={Documentos con diversidad de contenidos},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.4.3)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle
\tableofcontents



\section{Introducción.}
\label{sec:org0e1b712}

En esta sesión aprenderemos a editar un documento con todo su ecosistema o
\emph{esquema de relaciones} que se construyen a medida que el documento se crea.

Más allá de \emph{trabajar con un texto}, el documento en si presenta una \textbf{estructura}
que siempre es bueno tener clara para mejorar el proceso de producción de un
trabajo u obra. Esta estructura comprenderá archivos y carpetas que serán
asociadas en el documento principal correspondiente.

Dependiendo del caso, tendrás que crear menos o más \emph{carpetas} asociadas a cada
ítem. La idea es que dentro de proceso de creación de documentos o textos,
tendrás que llevar un orden de acuerdo a algún estándar o definición que sigas.
De esta manera harás más sencillo que otras personas puedan \emph{descubrir},
\emph{estudiar} o \emph{conocer} tu trabajo.

Podríamos decir que este estilo de edición se aproxima al flujo del
\emph{conocimiento libre y comunitario}. Es una metodología especialmente afinada
para movimientos culturales, científicos y tecnológicos que pretendan aportar al
desarrollo de la humanidad.

Una vez definida la estructura o estándar que sigas para la creación  de los
documentos del proyecto siempre es bueno dejar claro, con alguna anotación en el
documento principal o un archivo de estándar la estructura que seguirás.


\subsection{Una visión general.}
\label{sec:org37f2482}

A diferencia de los modos de escritura más conocidos, como los famosos \emph{word},
en que toda la información se empaqueta en una \emph{caja negra} o \emph{caja turbia}.
Dónde todo lo que ocurre adentro es prácticamente desconocido. El sistema de
edición de \textbf{org-mode} provee claridad y control sobre lo que creas.

Será necesario, como recomendación, que tu documento se aloje en un directorio y
los contenidos asociados estén ordenados. Será necesario pensar y definir, tal
como se ve en la figura \ref{docgeneral}.

\begin{itemize}
\item ¿dónde quedan las imágenes?
\item ¿dónde dejo los datos?
\item ¿dónde los ejemplos?
\item ¿dónde los ejercicios?
\item ¿dónde las referencias?
\end{itemize}

\subsubsection{¿cómo se hizo esta lista?}
\label{sec:org54aa45d}

Hay varias opciones que puedes tomar:

\begin{verbatim}
Lo que sigue aquí es una lista, y necesitas escribir
-(espacio)Texto
+(espacio)Texto
1)(espacio)Texto
Y el estilo puedes cambiarlo con:
Shift + (<- o ->)
\end{verbatim}

Según lo que necesitas puedes escoger la figura del 'bullet' a utilizar.

\subsubsection{Añadir imágenes y marcas de referencia}
\label{sec:orgba5d095}

Puedes seguir la siguiente estructura.

\begin{verbatim}
A continuación añadimos una imagen, con su 'codigo'
para citarla: 
#+NAME: 
El texto de figura:
#+CAPTION: 
Y atributos de tamaño para distintas exportaciones.
\end{verbatim}

En términos general veremos:

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/docgeneral.png}
\caption{\label{docgeneral}Ejemplo de estructura general para un documento.}
\end{figure}

Para enlazar correctamente una imagen al documento, será sencillo mediante la
siguiente expresión.

\begin{verbatim}
[[file:./img/docgeneral.png]]
\end{verbatim}


\subsection{Una visión particular}
\label{sec:orgbe48c78}

Una vez adentrados en la \emph{creación del texto}, con una \emph{estructura consistente},
se hará sencillo. Esta etapa puede ser la más divertida y creativa porque
requerirá el uso de todos los recursos posibles.

No es obligatorio que para todo uses \textbf{emacs} en este proceso, puedes crear
dibujos o diagramas con \textbf{Inkscape}, dibujos rápidos con \textbf{Openboard}, 
capturar pantalla, abrir \textbf{pdfs} con \textbf{Evince}, figuras geométricas con
\textbf{Geogebra}, usar navegadores web para buscar información, etcétera.

Lo importante es que cultives la habilidad de crear y recolectar la información
y los elementos adecuados para lograr una cierta belleza o armonía en la
creación del documento.

Algo muy sencillo de imaginar, es ver el proceso de edición como una pirámide
con tres etapas principales, como se ve en la figura \ref{etapas}.

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/etapas.png}
\caption{\label{etapas}Etapas de edición.}
\end{figure}

De esta manera, un documento se puede dividir en diferentes secciones que
expongan lo que quieras expresar, un ejemplo general de un detalle para un
documento org lo puedes observar en la imagen \ref{docorg}.

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/documento_org.png}
\caption{\label{docorg}Detalle de un org.}
\end{figure}

Antes de pensar en el \emph{producto final}, que será el documento al que accederán
las personas en primeras instancias (como se ve en \ref{etapas}), también es necesario
\textbf{recalcar} que el proceso de creación también es parte del producto. Por lo que
te recomiendo exponer o disponer del proyecto en algún repositorio de acceso
público. Esto aportará a las buenas prácticas y a la reflexión en torno a la
creación de los contenidos, sus métodos y formas de difusión.

\section{Etiquetas de uso común.}
\label{sec:org5495273}

En esta sección veremos diversos ejemplos prácticos de uso de las etiquetas más
comunes.

Todo lo que viene por defecto en \textbf{Org-mode} puede ser consultado en el \href{https://orgmode.org/manual/}{manual}.

\subsection{Enlaces web}
\label{sec:orgc99deb4}

Hay un par de canales bien buenos en \textbf{youtube} que tiene contenido de calida.

Uno que habla sobre desarrollo de videojuegos, es \textbf{Plano de Juego}, en dónde
\textbf{Champ} comenta sobre diversos videojuegos: su arte, la música, las formas de
jugar, etc.

Un capítulo muy recomendable es el que habla de \textbf{Hollow Knight}, lo puedes ver
en este \href{https://www.youtube.com/watch?v=UJqlreU0Y24}{comentario}.

También, he disfrutado mucho la música del canal de la radio \textbf{NPR}, en dónde
cuenta además la puesta en escena, la calidad de los/las invitados y el ambiente
que se genera. Uno muy bello es el que realizó \textbf{Dua Lipa} el año pasado, puedes
verlo en esta \href{https://www.youtube.com/watch?v=F4neLJQC1\_E}{presentación}

Con quienes hemos trabajado en los \textbf{Curso de programación} surgió la idea de
\textbf{curar} noticias y contenidos en un canal de \textbf{telegram}, así nació
\href{https://t.me/tecnocomunes\_seleccion}{Selecciones Tecnocomunes}, a las que te invito a subscribirte.

Para documentos cuyo destino sea la \textbf{web} o \textbf{públicos}, tal vez sea bueno
\emph{embeber} los videos. Si sigues esta \href{http://endlessparentheses.com/embedding-youtube-videos-with-org-mode-links.html}{guía} exitosamente seguro que podrás incluir
vídeos en tus documentos.

La clave:

\begin{verbatim}
[[URL][Texto]]
\end{verbatim}

\subsection{{\bfseries\sffamily TODO} Ejemplos}
\label{sec:orgd468fa1}

Cuando necesitas añadir un \textbf{ejemplo}, para mostrar alguna cosa específica,
puedes usar la etiqueta \textbf{begin\_example, end\_example} e incluir el texto entre
ellas.

También existe un \textbf{shortcut}:

\begin{verbatim}
C+c C+,
\end{verbatim}

Al seleccionar esta combinación, se desplegará un menú de snippets, como se ve
en figura \ref{snippets}.

\begin{figure}[htbp]
\centering
\includegraphics[width=400px]{./img/selector_snippets.png}
\caption{\label{snippets}Selección de snippets}
\end{figure}

Finalmente, para añadir un \emph{snippet de ejemplo}, apretar \textbf{e}.

\subsection{Citas}
\label{sec:org597e724}

Cuando necesites citar un texto específico, demarcando la diferencia, puedes
usar la marca \textbf{quote}.

\begin{quote}
Jake sacó la cartera y la abrió con nerviosismo, temeroso de haber salido de
casa con solo tres o cuatro dólares. Pero estaba de suerte. Llevaba un billeta
de cinco dólares y tres de uno. Le tendió el dinero a Torre, que plegó los
billetes y se los guardó despreocupadamente \ldots{}

Stepthen King. La torre oscura II, las tierras baldías.
\end{quote}

Se utilizan las siguientes etiquetas.

\begin{verbatim}
#+ATTR_TEXTINFO: :author AUTOR
#+begin_quote
<texto>
#+end_quote
\end{verbatim}

\begin{description}
\item[{Referencia}] \url{https://orgmode.org/manual/Quotations-in-Texinfo-export.html\#Quotations-in-Texinfo-export}
\end{description}

\subsection{Versos}
\label{sec:org2587196}

Para el caso de escribir poesía en verso, existe la etiqueta \textbf{verse}, que se
puede utilizar de manera similar a \textbf{quote}.

\phantomsection
\label{cita_nparra}
\begin{verse}
El hombre imaginario\\
vive en una mansión imaginaria\\
rodeada de árboles imaginarios\\
a la orilla de un rio imaginario.\\
.\\
.\\
\hspace*{4em}Nicanor Parra, El hombre imaginario\\
\end{verse}

En la cita \ref{cita_nparra} se leen las primeras líneas del reconocido poema \textbf{El
hombre imaginario} del chileno \textbf{Nicanor Parra}. 

\subsection{Listas}
\label{sec:org9ca9b87}

En general hay 3 tipos de listas que podrías manejar.

\begin{itemize}
\item listas ordenadas
\item listas no ordenadas
\item listas de definición.
\end{itemize}

Las listas no ordenadas se crean:

\begin{verbatim}
- perro
- gato
- vaca
\end{verbatim}

Las ordenadas, pueden ser numéricas o alfabéticas.

\begin{verbatim}
1) perro
2) gato
3) vaca
\end{verbatim}

O, si necesitamos usar definiciones conceptuales.

\begin{verbatim}
- perro :: animal mamífero, omnívoro, doméstico, nombre científico /Canis lupus familiaris/
- gato :: animal mamífero, omnívoro, doméstico, nombre científico /Felis silvestris catus/
- vaca :: animál mamífero, hervívoro, ganado, nombre científico /Bos taurus/
\end{verbatim}

En el editor \textbf{emacs} se refleja por:

\begin{description}
\item[{perro}] animal mamífero, omnívoro, doméstico, nombre científico \emph{Canis lupus familiaris}
\item[{gato}] animal mamífero, omnívoro, doméstico, nombre científico \emph{Felis silvestris catus}
\item[{vaca}] animál mamífero, hervívoro, ganado, nombre científico \emph{Bos taurus}
\end{description}

\subsection{Marcar itálicas y negritas}
\label{sec:org5ef987c}

Para \textbf{resaltar} trozos de texto podrás usar

\begin{description}
\item[{itálicas}] \emph{texto} \(/\) (barrita inclinada o de división)
\item[{negritas}] \textbf{texo} \(*\) (asterísco o multiplicación)
\end{description}


\subsection{Incluir ecuaciones o fórmulas matemáticas.}
\label{sec:orgd628a0c}

Como \textbf{Org-mode} se integra directamente con \textbf{Latex}. Se puede considerar una
versión suave o amable de este sistema de generación de documentos.

La inclusión de fórmulas es directa. Utilizando las marcas o palabras claves de
\textbf{latex}.

La forma directa, de incluir esta ecuación \(f(x) = x + 100\) es:

\begin{verbatim}
$f(x) = x + 100$
\end{verbatim}

O bien, para una visualización en todo el ancho de página.

\begin{equation}
Celsius(fahr) = \frac {5} {9} * (fahr  - 32) 
\end{equation}

De esta manera:

\begin{verbatim}
\begin{equation}
Celsius(fahr) = \frac {5} {9} * (fahr  - 32) 
\end{equation}
\end{verbatim}

Para visualizar ecuación como imagen, debes apretar. Si quieres volver a editar también.

\begin{verbatim}
C+c C+x C+l
\end{verbatim}

Debes tener instalada la funcionalidad \textbf{dvisvgm}.


\subsection{Manejo de calendario.}
\label{sec:org48bf231}

Para seleccionar alguna fecha específica, si bien puedes escribirla
directamente, también puedes seleccionar mediante el \emph{menú de calendario}.

\begin{verbatim}
C+c .
\end{verbatim}

Y puedes seleccionar una fecha como esta:

\begin{itemize}
\item \textit{<2021-05-29 sáb>}
\end{itemize}

Para navegar entre los meses puedes apretar \textbf{Shift+ (<-, ->)}.

\subsection{Tareas por hacer (TODO - DONE).}
\label{sec:org9e0dffc}

Así como sistema de creación de documentos, en \textbf{Org-mode} es posible gestionar
tareas por hacer. Del inglés \textbf{to do}, la etiqueta \textbf{TODO} se puede colocar al
inicio de una marca de título y demarcar su desarrollo bajo distintos estados.

\subsubsection{{\bfseries\sffamily TODO} tarea por hacer}
\label{sec:org221d207}

operador: Juanito Palote

\subsubsection{{\bfseries\sffamily DONE} tarea ya realizada}
\label{sec:org3b99496}
operador: Augusta Pinchetty

Para cambiar el \textbf{estado} de la \emph{tarea}, basta con usar \textbf{Shift+(<- o ->)}.

\subsection{Pie de página.}
\label{sec:orge9f1cf6}

Si necesitas dejar un pie de página, puedes reverenciarlo con 

\begin{verbatim}
The Org homepage[fn:1] now looks a lot better than it used to.
...
[fn:1] The link is: https://orgmode.org
\end{verbatim}

De esta manera, si estoy hablando de la \textbf{web de Orgmode\footnote{La url es: \url{https://orgmode.org}}} y necesito
mostrarte el enlace.

\section{Tablas}
\label{sec:org37dba8c}

Las tablas son una de las \textbf{características} más interesantes que nos ofrece
\textbf{Org-mode}

\subsection{¿Qué es una tabla?}
\label{sec:org6b852ea}

Es un elemento visual con \textbf{filas} y \textbf{columnas}, cada recuadro que define se le
llama \textbf{celda}. Sirve para ordenar y contener información que sea fácil de
ubicar. La primera fila se puede considerar o llamar el \textbf{encabezado}, que
contendrá los nombres o definición del tipo de dato de cada columna.

\subsection{Crear una tabla}
\label{sec:org28aabff}

Si bien puedes escribirla \textbf{sin atajos de teclado}, crear una tabla te llevará un
segundo:

\begin{verbatim}
C+c | 
\end{verbatim}

Si escojo \textbf{5x3}, tendremos cinco columnas y tres filas.

\begin{verbatim}
|   |   |   |   |   |
|---+---+---+---+---|
|   |   |   |   |   |
|   |   |   |   |   |
\end{verbatim}

Si al escribir cambia el tamaño de la tabla puedes apretar \textbf{C+c C+c} para
ajustar el ancho de las columnas. También te servirá activar el \textbf{valign-mode}
desde \textbf{M+x} (alt y x).

\subsection{Posicionar filas o columnas.}
\label{sec:org15775fa}

Dada una tabla.

\begin{center}
\begin{tabular}{lrrrr}
nombre & edad & herman@s & ti@s & padres\\
\hline
José & 13 & 2 & 3 & 1\\
Daniela & 12 & 6 & 6 & 2\\
María & 14 & 6 & 1 & 1\\
\end{tabular}
\end{center}

Que se completa a mano o de alguna fuente (copiando de un archivo)

\begin{verbatim}
| nombre  | edad | herman@s | ti@s | padres |
|---------+------+----------+------+--------|
| José    |   13 |        2 |    3 |      1 |
| Daniela |   12 |        6 |    6 |      2 |
| María   |   14 |        6 |    1 |      1 |
\end{verbatim}

Si queremos mover \textbf{Mearía} al primer lugar.

\begin{verbatim}
M+(arriba)
M+(arriba)
\end{verbatim}

Tendremos

\begin{center}
\begin{tabular}{lrrrr}
nombre & edad & herman@s & ti@s & padres\\
\hline
María & 14 & 6 & 1 & 1\\
José & 13 & 2 & 3 & 1\\
Daniela & 12 & 6 & 6 & 2\\
\end{tabular}
\end{center}


Si queremos mover la columna \textbf{edad} lo más a la derecha. Nos ubicamos en la
columna correspondiente.

\begin{verbatim}
M+(derecha)
M+(derecha)
M+(derecha)
\end{verbatim}

Resulta

\begin{center}
\begin{tabular}{lrrrr}
nombre & herman@s & ti@s & padres & edad\\
\hline
María & 6 & 1 & 1 & 14\\
José & 2 & 3 & 1 & 13\\
Daniela & 6 & 6 & 2 & 12\\
\end{tabular}
\end{center}

\subsubsection{Pregunta}
\label{sec:org725771d}

¿Cuánto tiempo demorarías en hacer esto en un editor de texto común?

\subsection{Añadir y borrar filas/columnas}
\label{sec:org4a4158e}

Esta técnica de edición de tablas permite modificar el tamaño de la tabla.

Iniciamos con tabla:

\begin{verbatim}
|   |   |   |   |   |
|---+---+---+---+---|
|   |   |   |   |   |
|   |   |   |   |   |
\end{verbatim}

Para agregar filas

\begin{verbatim}
M+S+(abajo)
|   |   |   |   |   |
|---+---+---+---+---|
|   |   |   |   |   |
|   |   |   |   |   |
|   |   |   |   |   |
\end{verbatim}

Para quitar.

\begin{verbatim}
M+S+(arriba)
|   |   |   |   |   |
|---+---+---+---+---|
|   |   |   |   |   |
|   |   |   |   |   |
\end{verbatim}

Para el caso de columnas. En vez de movernos (arriba-abajo), será
(izquierda-derecha).

Añadir columna
\begin{verbatim}
M+S+(derecha)
|   |   |   |   |   |   |
|---+---+---+---+---+---|
|   |   |   |   |   |   |
|   |   |   |   |   |   |
\end{verbatim}

Quitar 2 columnas, quedamos con:

\begin{verbatim}
M+S+(izquierda)
M+S+(izquierda)
|   |   |   |   |
|---+---+---+---|
|   |   |   |   |
|   |   |   |   |
\end{verbatim}

Terminamos en:

\begin{center}
\begin{tabular}{llll}
a & d & e & f\\
\hline
b &  &  & \\
c &  &  & \\
\end{tabular}
\end{center}


\subsection{Evaluar valores de columnas}
\label{sec:orgb5a85b6}

Tal como en diferentes planillas de datos veremos formas de operar con la
información disponible en la tabla.

\subsection{Importar desde csv}
\label{sec:org3922f07}

Dado un \textbf{csv} que contenga datos, podemos cargar datos en una tabla, con la
siguiente acción.

\begin{enumerate}
\item Copiamos y pegamos en el documento \textbf{org}
\item Apretamos \(C+c \: |\) para generar la tabla sobre el texto del \emph{csv}.
\item Añadimos línea separadora del \textbf{encabezado} \(C+c\:-\)
\end{enumerate}

\begin{table}[htbp]
\label{tabla_csv}
\centering
\begin{tabular}{lr}
dia & frente\\
\hline
lunes & 60\\
martes & 55\\
miércoles & 70\\
jueves & 66\\
viernes & 34\\
sábado & 45\\
domingo & 40\\
\end{tabular}
\end{table}

\begin{enumerate}
\item Agregamos nueva tabla \textbf{celsius}.
\end{enumerate}

\begin{table}[htbp]
\label{tabla_con_col_celsius}
\centering
\begin{tabular}{lrl}
dia & fahrenheit & celsius\\
\hline
lunes & 60 & \\
martes & 55 & \\
miércoles & 70 & \\
jueves & 66 & \\
viernes & 34 & \\
domingo & 40 & \\
sábado & 45 & \\
\end{tabular}
\end{table}

Exportamos, o leemos la tabla en algún lenguaje, por ejemplo \textbf{elisp}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{elisp}
(orgtbl-to-csv x nil)
\end{minted}

También, podemos guardar los datos a un archivo \textbf{csv}. Esto porque hemos
generado más información en base  a los datos base.

Apretamos.

\begin{verbatim}
M+m m t E
\end{verbatim}

Luego debemos entregar el nombre del archivo a guardar.

\subsection{Realizar cálculos en la tabla}
\label{sec:orgba20f01}

Tal como las operaciones de funciones en \textbf{planillas de cálculo}, en las tablas
podemos realizar operaciones sobre las celdas (filas-columnas).

\begin{table}[htbp]
\label{tabla_celsius_calc}
\centering
\begin{tabular}{lrr}
dia & fahrenheit & celsius\\
\hline
lunes & 60 & 15.555556\\
martes & 55 & \\
miércoles & 70 & \\
jueves & 66 & \\
viernes & 34 & \\
domingo & 40 & \\
sábado & 45 & \\
\end{tabular}
\end{table}

Las filas se referencias con:
\begin{itemize}
\item @fila
\end{itemize}

Las columnas con '\$'
\begin{itemize}
\item \$columna
\end{itemize}

Para activar/desactivar el \textbf{posicionamiento referencial}, lo activamos con \textbf{C-c \}}

Para activar/desactivar el \textbf{debugging} de las tablas, \textbf{C-c \{}.

Debemos implementar en la tabla la siguiente fórmula de conversión.

\begin{equation}
Celsius(fahr) = \frac {5} {9} * (fahr  - 32) 
\end{equation}

Sería, ubicándonos en la primera celda celsius, escribimos.

\begin{verbatim}
:= (5/9) * ($2 -32)
\end{verbatim}

Apretamos para realizar el cálculo.

\begin{verbatim}
C+c C+c -
\end{verbatim}

Necesitamos, además, que la fórmula sea aplicada a toda la columna, usaremos
solo = en vez de \textbf{:=}.

\begin{table}[htbp]
\label{tabla_celsius_calc_column}
\centering
\begin{tabular}{lrr}
dia & fahrenheit & celsius\\
\hline
lunes & 60 & 15.555556\\
martes & 55 & 12.777778\\
miércoles & 70 & 21.111111\\
jueves & 66 & 18.888889\\
viernes & 34 & 1.1111111\\
domingo & 40 & 4.4444444\\
sábado & 45 & 7.2222222\\
\end{tabular}
\end{table}

Para aplicarla a toda la columna.

\begin{verbatim}
C+c C+c *
\end{verbatim}

O, si estás en la etiqueta \textbf{TBLFM}, basta con.

\begin{verbatim}
C+c C+c
\end{verbatim}

\subsection{Editar fórmulas de manera interactiva}
\label{sec:org72442f8}

Se puede abrir un \textbf{minibuffer} para editar las fórmulas que se aplican en la
tabla. Ubicado en el borde inferior del editor.

Para fórmulas de columna
\begin{verbatim}
C+c =
\end{verbatim}

Para fórmulas de campo.

\begin{verbatim}
C+u C+c =
\end{verbatim}

¿Qué pasa si nos piden agregar la conversión a \textbf{kelvin}?

\begin{table}[htbp]
\label{tabla_celsius_calc_column}
\centering
\begin{tabular}{lrrr}
dia & fahrenheit & celsius & kelvin\\
\hline
lunes & 60 & 15.555556 & 288.70556\\
martes & 55 & 12.777778 & 285.92778\\
miércoles & 70 & 21.111111 & 294.26111\\
jueves & 66 & 18.888889 & 292.03889\\
viernes & 34 & 1.1111111 & 274.26111\\
domingo & 40 & 4.4444444 & 277.59444\\
sábado & 45 & 7.2222222 & 280.37222\\
\end{tabular}
\end{table}

También, para editar las fórmulas en general, puedes acceder al \textbf{buffer editor de fórmulas}

\begin{verbatim}
C+c '
\end{verbatim}

Y cerrar con, guardando y evulando las fórmulas.

\begin{verbatim}
C+c C+c
\end{verbatim}

\begin{description}
\item[{Más info}] \url{https://orgmode.org/manual/Editing-and-debugging-formulas.html}
\end{description}

\section{Citas referenciales}
\label{sec:orgd171d3c}

Para citar documentos que haz utilizado, tendrás que configurar \textbf{org-ref}.

El repositorio lo encuentras \href{https://github.com/jkitchin/org-ref}{acá}

También, de manera directa con \textbf{Spacemacs}, podrás activar la \textbf{layer Bibtex}
según esta \href{https://develop.spacemacs.org/layers/+lang/bibtex/README.html}{guía}.

Puedes añadir la configuración, añadiendo lo siguiente en el \textbf{.spacemacs}, en el
bloque \textbf{user-config}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{common-lisp}
(setq org-ref-default-bibliography '("~/Papers/references.bib")
      org-ref-pdf-directory "~/Papers/"
      org-ref-bibliography-notes "~/Papers/notes.org")
\end{minted}

Y las citas se hacen utilizando el mismo estilo de \textbf{latex} (o marca).

\begin{verbatim}
- Esto es una cita \cite{Tappert77}.
\end{verbatim}
\end{document}
