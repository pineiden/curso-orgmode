#+TITLE: Creación de libro escaneado

* Introducción.

Este es un ejercicio de ejemplo de lo que sería la /programación literaria/ e
/investigación reproducible/.

** ¿Dónde estamos?

#+begin_src shell
pwd
#+end_src

#+RESULTS:
: /home/david/Documentos/org_mode_curso_broadcast/crearlibro


Luego voy a listar los directorios.


#+begin_src shell
ls
#+end_src

#+RESULTS:
| Libro              |
| cap16              |
| creacion_libro.org |
| libro_scan.zip     |
| pngs               |



Vemos que hay en *Libro*, todos los archivos de adentro del directorio.

#+begin_src shell
ls Libro/*
#+end_src

#+RESULTS:
| Libro/PNJ108-109.pnm    |
| Libro/contraportada.pnm |
| Libro/ong138-139.pnm    |
| Libro/out.pnm           |
| Libro/pang18-19.pnm     |
| Libro/pang28-29.pnm     |
| Libro/pg214-215.pnm     |
| Libro/pgm128-129.pnm    |
| Libro/pgn116-117.pnm    |
| Libro/pgn12-13.pnm      |
| Libro/pgn128-129.pnm    |
| Libro/pgn132-133.pnm    |
| Libro/pgn136-137.pnm    |
| Libro/pgn136-137b.pnm   |
| Libro/pgn14-15.pnm      |
| Libro/pgn16-17.pnm      |
| Libro/pgn162-163.pnm    |
| Libro/pgn164-165.pnm    |
| Libro/pgn166-167.pnm    |
| Libro/pgn170-171.pnm    |
| Libro/pgn172-173.pnm    |
| Libro/pgn174.175.pnm    |
| Libro/pgn176-177.pnm    |
| Libro/pgn178-179.pnm    |
| Libro/pgn180-181.pnm    |
| Libro/pgn182-183.pnm    |
| Libro/pgn184-185.pnm    |
| Libro/pgn186-187.pnm    |
| Libro/pgn188-189.pnm    |
| Libro/pgn190-191.pnm    |
| Libro/pgn192-193.pnm    |
| Libro/pgn194-195.pnm    |
| Libro/pgn196-197.pnm    |
| Libro/pgn198-199.pnm    |
| Libro/pgn202-203.pnm    |
| Libro/pgn204-204.pnm    |
| Libro/pgn204-205.pnm    |
| Libro/pgn206-207.pnm    |
| Libro/pgn208-209.pnm    |
| Libro/pgn210-211.pnm    |
| Libro/pgn212-213.pnm    |
| Libro/pgn216-217.pnm    |
| Libro/pgn218-219.pnm    |
| Libro/pgn22-23.pnm      |
| Libro/pgn220-221.pnm    |
| Libro/pgn222-223.pnm    |
| Libro/pgn224-225.pnm    |
| Libro/pgn226-227.pnm    |
| Libro/pgn228-229.pnm    |
| Libro/pgn230-231.pnm    |
| Libro/pgn232-233.pnm    |
| Libro/pgn234-235.pnm    |
| Libro/pgn236-237.pnm    |
| Libro/pgn238-239.pnm    |
| Libro/pgn24-25.pnm      |
| Libro/pgn240-241.pnm    |
| Libro/pgn242-243.pnm    |
| Libro/pgn244-245.pnm    |
| Libro/pgn246-247.pnm    |
| Libro/pgn248-249.pnm    |
| Libro/pgn250-251.pnm    |
| Libro/pgn252-253.pnm    |
| Libro/pgn254-255.pnm    |
| Libro/pgn256-257.pnm    |
| Libro/pgn26-27.pnm      |
| Libro/pgn260-261.pnm    |
| Libro/pgn262-263.pnm    |
| Libro/pgn264-265.pnm    |
| Libro/pgn266-267.pnm    |
| Libro/pgn268-269.pnm    |
| Libro/pgn270-271.pnm    |
| Libro/pgn272-273.pnm    |
| Libro/pgn274-275.pnm    |
| Libro/pgn276-277.pnm    |
| Libro/pgn278-279.pnm    |
| Libro/pgn28-29.pnm      |
| Libro/pgn280-281.pnm    |
| Libro/pgn282-283.pnm    |
| Libro/pgn284-285.pnm    |
| Libro/pgn286-287.pnm    |
| Libro/pgn288-289.pnm    |
| Libro/pgn290-291.pnm    |
| Libro/pgn292-293.pnm    |
| Libro/pgn294-295.pnm    |
| Libro/pgn296-297.pnm    |
| Libro/pgn298-299.pnm    |
| Libro/pgn30-31.pnm      |
| Libro/pgn300-301.pnm    |
| Libro/pgn302-303.pnm    |
| Libro/pgn304-305.pnm    |
| Libro/pgn306-307.pnm    |
| Libro/pgn308-309.pnm    |
| Libro/pgn310-311.pnm    |
| Libro/pgn312-313.pnm    |
| Libro/pgn314-315.pnm    |
| Libro/pgn316-317.pnm    |
| Libro/pgn318-319.pnm    |
| Libro/pgn320-321.pnm    |
| Libro/pgn322-323.pnm    |
| Libro/pgn324-325.pnm    |
| Libro/pgn326-327.pnm    |
| Libro/pgn328-329.pnm    |
| Libro/pgn33-34.pnm      |
| Libro/pgn330-331.pnm    |
| Libro/pgn332-333.pnm    |
| Libro/pgn334-335.pnm    |
| Libro/pgn334.pnm        |
| Libro/pgn336-337.pnm    |
| Libro/pgn338-339.pnm    |
| Libro/pgn340-341.pnm    |
| Libro/pgn342-343.pnm    |
| Libro/pgn344-345.pnm    |
| Libro/pgn346-347.pnm    |
| Libro/pgn348-349.pnm    |
| Libro/pgn35-36.pnm      |
| Libro/pgn350-351.pnm    |
| Libro/pgn352-353.pnm    |
| Libro/pgn354-355.pnm    |
| Libro/pgn356-357.pnm    |
| Libro/pgn358-359.pnm    |
| Libro/pgn360-361.pnm    |
| Libro/pgn362-363.pnm    |
| Libro/pgn364-365.pnm    |
| Libro/pgn366-367.pnm    |
| Libro/pgn368-369.pnm    |
| Libro/pgn370-371.pnm    |
| Libro/pgn372-373.pnm    |
| Libro/pgn374-375.pnm    |
| Libro/pgn376-377.pnm    |
| Libro/pgn378-379.pnm    |
| Libro/pgn380-381.pnm    |
| Libro/pgn382-383pnm     |
| Libro/pgn384-385.pnm    |
| Libro/pgn386-387.pnm    |
| Libro/pgn388-389.pnm    |
| Libro/pgn390-391.pnm    |
| Libro/pgn392-393.pnm    |
| Libro/pgn394-395.pnm    |
| Libro/pgn396-397.pnm    |
| Libro/pgn398-399.pnm    |
| Libro/pgn400-401.pnm    |
| Libro/pgn402-403.pnm    |
| Libro/pgn404-405.pnm    |
| Libro/pgn406-407.pnm    |
| Libro/pgn408-409.pnm    |
| Libro/pgn410-411.pnm    |
| Libro/pgn412-413.pnm    |
| Libro/pgn414-415.pnm    |
| Libro/pgn416-417.pnm    |
| Libro/pgn418-419.pnm    |
| Libro/pgn42-43.pnm      |
| Libro/pgn420-421.pnm    |
| Libro/pgn422-423.pnm    |
| Libro/pgn424-425.pnm    |
| Libro/pgn426-427.pnm    |
| Libro/pgn428-429.pnm    |
| Libro/pgn430-431.pnm    |
| Libro/pgn432-433.pnm    |
| Libro/pgn434-435.pnm    |
| Libro/pgn436-437.pnm    |
| Libro/pgn438-439.pnm    |
| Libro/pgn440-441.pnm    |
| Libro/pgn442-443.pnm    |
| Libro/pgn444-445.pnm    |
| Libro/pgn446-447.pnm    |
| Libro/pgn448-449.pnm    |
| Libro/pgn52-53.pnm      |
| Libro/pgn74-75.pnm      |
| Libro/pgn81-82.pnm      |
| Libro/pgn82-83.pnm      |
| Libro/pgn90-91.pnm      |
| Libro/pgn98-99.pnm      |
| Libro/pgn_000-001.pnm   |
| Libro/pgn_002-003.pnm   |
| Libro/pgn_004-005.pnm   |
| Libro/pgn_006-007.pnm   |
| Libro/pgn_008-009.pnm   |
| Libro/pgn_010-011.pnm   |
| Libro/pgna200-201.pnm   |
| Libro/pgna258-259.pnm   |
| Libro/pgna266-267.pnm   |
| Libro/pjn110-111.pnm    |
| Libro/pjn112-113.pnm    |
| Libro/pjn114-115.pnm    |
| Libro/pjn116-117.pnm    |
| Libro/pjn118-119.pnm    |
| Libro/pjn120-121.pnm    |
| Libro/pjn124-125.pnm    |
| Libro/pjn126-127.pnm    |
| Libro/pjn128-129.pnm    |
| Libro/pjn130-131.pnm    |
| Libro/pjn132-133.pnm    |
| Libro/pmg22-23.pnm      |
| Libro/png100-101.pnm    |
| Libro/png102-103.pnm    |
| Libro/png12-13.pnm      |
| Libro/png134-135.pnm    |
| Libro/png136-137.pnm    |
| Libro/png14-15.pnm      |
| Libro/png140-141.pnm    |
| Libro/png142-143.pnm    |
| Libro/png144-145.pnm    |
| Libro/png146-147.pnm    |
| Libro/png148-149.pnm    |
| Libro/png150-151.pnm    |
| Libro/png152-153.pnm    |
| Libro/png154-155.pnm    |
| Libro/png156-157.pnm    |
| Libro/png158-159.pnm    |
| Libro/png160-161.pnm    |
| Libro/png164-165.pnm    |
| Libro/png166-167.pnm    |
| Libro/png168-169.pnm    |
| Libro/png170-171.pnm    |
| Libro/png18-19.pnm      |
| Libro/png22-23.pnm      |
| Libro/png24-25.pnm      |
| Libro/png26-27.pnm      |
| Libro/png30-31.pnm      |
| Libro/png348-349.pnm    |
| Libro/png385-386.pnm    |
| Libro/png39-40.pnm      |
| Libro/png41-42.pnm      |
| Libro/png52-53.pnm      |
| Libro/png54-55.pnm      |
| Libro/png56-57.pnm      |
| Libro/png58-59.pnm      |
| Libro/png60-61.pnm      |
| Libro/png62-63.pnm      |
| Libro/png64-65.pnm      |
| Libro/png68-69.pnm      |
| Libro/png70-71.pnm      |
| Libro/png72-73.pnm      |
| Libro/png74-75.pnm      |
| Libro/png76-77.pnm      |
| Libro/png78-79.pnm      |
| Libro/png80-81.pnm      |
| Libro/png82-83.pnm      |
| Libro/png84-85.pnm      |
| Libro/png90-91.pnm      |
| Libro/png92-93.pnm      |
| Libro/png94-95.pnm      |
| Libro/png96-97.pnm      |
| Libro/png98-99.pnm      |
| Libro/pnj106-107.pnm    |
| Libro/portada.pnm       |
|                         |
| Libro/horizontal:       |
| pgn18-19.pnm            |
| pgn39-40.pnm            |
| pgn41-42.pnm            |
| pgn80-81.pnm            |
| pgn_024-025.pnm         |
| pgn_026-027.pnm         |
| pgn_030-031.pnm         |
| pgn_033-34.pnm          |
| pgn_28-29.pnm           |
| pgn_35-36.pnm           |
| pgn_37-38.pnm           |
| pgn_41-42.pnm           |
| pgn_52-53.pnm           |
|                         |
| Libro/mono:             |
| pang20.pnm              |
| pgn1.png                |
| pgn2.png                |
| pgn3.png                |
| pgn32.pnm               |
| pgn4.png                |
| pgn5.pnm                |
| pgn9.png                |
| pgn9.pnm                |
| pgn_20.pnm              |
| png10.pnm               |
| png11.pnm               |
| png21.pnm               |
| png6.pnm                |
| png7.pnm                |
| png8.pnm                |

Estos son los datos, como los arreglamos?


Encontrar patrones o los grupos diferentes.

Patrón básico

#+begin_example
PAGINA - NUMERO IZQ NUMERO DER . formato
#+end_example

Mostremos una imagen, para ver como están presentadas.

El escaneo del libro resultó en imágenes blanco y negro, en vertical.

Tiene el grupo distintas codificaciones de archivo.

#+attr_org: :width 150px
[[file:./Libro/PNJ108-109.pnm]]

** ¿Cómo encontraría los patrones diferentes?

- awk
- substr :: ESTO ES UN TEXTO -> ESTO
- awk

#+begin_src shell :dir ./Libro
pwd
#+end_src

#+RESULTS:
: /home/david/Documentos/org_mode_curso_broadcast/crearlibro/Libro

Listamos todos  los pnm

#+begin_src shell :dir ./Libro
ls *.pnm
#+end_src

#+RESULTS:
| PNJ108-109.pnm    |
| contraportada.pnm |
| ong138-139.pnm    |
| out.pnm           |
| pang18-19.pnm     |
| pang28-29.pnm     |
| pg214-215.pnm     |
| pgm128-129.pnm    |
| pgn116-117.pnm    |
| pgn12-13.pnm      |
| pgn128-129.pnm    |
| pgn132-133.pnm    |
| pgn136-137.pnm    |
| pgn136-137b.pnm   |
| pgn14-15.pnm      |
| pgn16-17.pnm      |
| pgn162-163.pnm    |
| pgn164-165.pnm    |
| pgn166-167.pnm    |
| pgn170-171.pnm    |
| pgn172-173.pnm    |
| pgn174.175.pnm    |
| pgn176-177.pnm    |
| pgn178-179.pnm    |
| pgn180-181.pnm    |
| pgn182-183.pnm    |
| pgn184-185.pnm    |
| pgn186-187.pnm    |
| pgn188-189.pnm    |
| pgn190-191.pnm    |
| pgn192-193.pnm    |
| pgn194-195.pnm    |
| pgn196-197.pnm    |
| pgn198-199.pnm    |
| pgn202-203.pnm    |
| pgn204-204.pnm    |
| pgn204-205.pnm    |
| pgn206-207.pnm    |
| pgn208-209.pnm    |
| pgn210-211.pnm    |
| pgn212-213.pnm    |
| pgn216-217.pnm    |
| pgn218-219.pnm    |
| pgn22-23.pnm      |
| pgn220-221.pnm    |
| pgn222-223.pnm    |
| pgn224-225.pnm    |
| pgn226-227.pnm    |
| pgn228-229.pnm    |
| pgn230-231.pnm    |
| pgn232-233.pnm    |
| pgn234-235.pnm    |
| pgn236-237.pnm    |
| pgn238-239.pnm    |
| pgn24-25.pnm      |
| pgn240-241.pnm    |
| pgn242-243.pnm    |
| pgn244-245.pnm    |
| pgn246-247.pnm    |
| pgn248-249.pnm    |
| pgn250-251.pnm    |
| pgn252-253.pnm    |
| pgn254-255.pnm    |
| pgn256-257.pnm    |
| pgn26-27.pnm      |
| pgn260-261.pnm    |
| pgn262-263.pnm    |
| pgn264-265.pnm    |
| pgn266-267.pnm    |
| pgn268-269.pnm    |
| pgn270-271.pnm    |
| pgn272-273.pnm    |
| pgn274-275.pnm    |
| pgn276-277.pnm    |
| pgn278-279.pnm    |
| pgn28-29.pnm      |
| pgn280-281.pnm    |
| pgn282-283.pnm    |
| pgn284-285.pnm    |
| pgn286-287.pnm    |
| pgn288-289.pnm    |
| pgn290-291.pnm    |
| pgn292-293.pnm    |
| pgn294-295.pnm    |
| pgn296-297.pnm    |
| pgn298-299.pnm    |
| pgn30-31.pnm      |
| pgn300-301.pnm    |
| pgn302-303.pnm    |
| pgn304-305.pnm    |
| pgn306-307.pnm    |
| pgn308-309.pnm    |
| pgn310-311.pnm    |
| pgn312-313.pnm    |
| pgn314-315.pnm    |
| pgn316-317.pnm    |
| pgn318-319.pnm    |
| pgn320-321.pnm    |
| pgn322-323.pnm    |
| pgn324-325.pnm    |
| pgn326-327.pnm    |
| pgn328-329.pnm    |
| pgn33-34.pnm      |
| pgn330-331.pnm    |
| pgn332-333.pnm    |
| pgn334-335.pnm    |
| pgn334.pnm        |
| pgn336-337.pnm    |
| pgn338-339.pnm    |
| pgn340-341.pnm    |
| pgn342-343.pnm    |
| pgn344-345.pnm    |
| pgn346-347.pnm    |
| pgn348-349.pnm    |
| pgn35-36.pnm      |
| pgn350-351.pnm    |
| pgn352-353.pnm    |
| pgn354-355.pnm    |
| pgn356-357.pnm    |
| pgn358-359.pnm    |
| pgn360-361.pnm    |
| pgn362-363.pnm    |
| pgn364-365.pnm    |
| pgn366-367.pnm    |
| pgn368-369.pnm    |
| pgn370-371.pnm    |
| pgn372-373.pnm    |
| pgn374-375.pnm    |
| pgn376-377.pnm    |
| pgn378-379.pnm    |
| pgn380-381.pnm    |
| pgn384-385.pnm    |
| pgn386-387.pnm    |
| pgn388-389.pnm    |
| pgn390-391.pnm    |
| pgn392-393.pnm    |
| pgn394-395.pnm    |
| pgn396-397.pnm    |
| pgn398-399.pnm    |
| pgn400-401.pnm    |
| pgn402-403.pnm    |
| pgn404-405.pnm    |
| pgn406-407.pnm    |
| pgn408-409.pnm    |
| pgn410-411.pnm    |
| pgn412-413.pnm    |
| pgn414-415.pnm    |
| pgn416-417.pnm    |
| pgn418-419.pnm    |
| pgn42-43.pnm      |
| pgn420-421.pnm    |
| pgn422-423.pnm    |
| pgn424-425.pnm    |
| pgn426-427.pnm    |
| pgn428-429.pnm    |
| pgn430-431.pnm    |
| pgn432-433.pnm    |
| pgn434-435.pnm    |
| pgn436-437.pnm    |
| pgn438-439.pnm    |
| pgn440-441.pnm    |
| pgn442-443.pnm    |
| pgn444-445.pnm    |
| pgn446-447.pnm    |
| pgn448-449.pnm    |
| pgn52-53.pnm      |
| pgn74-75.pnm      |
| pgn81-82.pnm      |
| pgn82-83.pnm      |
| pgn90-91.pnm      |
| pgn98-99.pnm      |
| pgn_000-001.pnm   |
| pgn_002-003.pnm   |
| pgn_004-005.pnm   |
| pgn_006-007.pnm   |
| pgn_008-009.pnm   |
| pgn_010-011.pnm   |
| pgna200-201.pnm   |
| pgna258-259.pnm   |
| pgna266-267.pnm   |
| pjn110-111.pnm    |
| pjn112-113.pnm    |
| pjn114-115.pnm    |
| pjn116-117.pnm    |
| pjn118-119.pnm    |
| pjn120-121.pnm    |
| pjn124-125.pnm    |
| pjn126-127.pnm    |
| pjn128-129.pnm    |
| pjn130-131.pnm    |
| pjn132-133.pnm    |
| pmg22-23.pnm      |
| png100-101.pnm    |
| png102-103.pnm    |
| png12-13.pnm      |
| png134-135.pnm    |
| png136-137.pnm    |
| png14-15.pnm      |
| png140-141.pnm    |
| png142-143.pnm    |
| png144-145.pnm    |
| png146-147.pnm    |
| png148-149.pnm    |
| png150-151.pnm    |
| png152-153.pnm    |
| png154-155.pnm    |
| png156-157.pnm    |
| png158-159.pnm    |
| png160-161.pnm    |
| png164-165.pnm    |
| png166-167.pnm    |
| png168-169.pnm    |
| png170-171.pnm    |
| png18-19.pnm      |
| png22-23.pnm      |
| png24-25.pnm      |
| png26-27.pnm      |
| png30-31.pnm      |
| png348-349.pnm    |
| png385-386.pnm    |
| png39-40.pnm      |
| png41-42.pnm      |
| png52-53.pnm      |
| png54-55.pnm      |
| png56-57.pnm      |
| png58-59.pnm      |
| png60-61.pnm      |
| png62-63.pnm      |
| png64-65.pnm      |
| png68-69.pnm      |
| png70-71.pnm      |
| png72-73.pnm      |
| png74-75.pnm      |
| png76-77.pnm      |
| png78-79.pnm      |
| png80-81.pnm      |
| png82-83.pnm      |
| png84-85.pnm      |
| png90-91.pnm      |
| png92-93.pnm      |
| png94-95.pnm      |
| png96-97.pnm      |
| png98-99.pnm      |
| pnj106-107.pnm    |
| portada.pnm       |


Ahora seleccionamos aquellos que en su nombre tengan "-"

O bien, mejor creamos carpeta para los archivos de portada y contraportada.

#+begin_src shell :dir ./Libro
mkdir -p portadas
mv portada.pnm portadas
mv contraportada.pnm portadas
rm out.pnm
#+end_src

#+RESULTS:

#+begin_src shell :dir ./Libro :results output
ls portadas
#+end_src

#+RESULTS:
: contraportada.pnm
: portada.pnm


Ahora, podemos tomar todos los archivos de página


#+begin_src shell :dir ./Libro
ls *.pnm | awk '{cabeza=substr($0,1,3); extra=substr($0,4,2); print cabeza, extra}'
#+end_src

#+RESULTS:
| PNJ | 10 |
| ong | 13 |
| pan | g1 |
| pan | g2 |
| pg2 | 14 |
| pgm | 12 |
| pgn | 11 |
| pgn | 12 |
| pgn | 12 |
| pgn | 13 |
| pgn | 13 |
| pgn | 13 |
| pgn | 14 |
| pgn | 16 |
| pgn | 16 |
| pgn | 16 |
| pgn | 16 |
| pgn | 17 |
| pgn | 17 |
| pgn | 17 |
| pgn | 17 |
| pgn | 17 |
| pgn | 18 |
| pgn | 18 |
| pgn | 18 |
| pgn | 18 |
| pgn | 18 |
| pgn | 19 |
| pgn | 19 |
| pgn | 19 |
| pgn | 19 |
| pgn | 19 |
| pgn | 20 |
| pgn | 20 |
| pgn | 20 |
| pgn | 20 |
| pgn | 20 |
| pgn | 21 |
| pgn | 21 |
| pgn | 21 |
| pgn | 21 |
| pgn | 22 |
| pgn | 22 |
| pgn | 22 |
| pgn | 22 |
| pgn | 22 |
| pgn | 22 |
| pgn | 23 |
| pgn | 23 |
| pgn | 23 |
| pgn | 23 |
| pgn | 23 |
| pgn | 24 |
| pgn | 24 |
| pgn | 24 |
| pgn | 24 |
| pgn | 24 |
| pgn | 24 |
| pgn | 25 |
| pgn | 25 |
| pgn | 25 |
| pgn | 25 |
| pgn | 26 |
| pgn | 26 |
| pgn | 26 |
| pgn | 26 |
| pgn | 26 |
| pgn | 26 |
| pgn | 27 |
| pgn | 27 |
| pgn | 27 |
| pgn | 27 |
| pgn | 27 |
| pgn | 28 |
| pgn | 28 |
| pgn | 28 |
| pgn | 28 |
| pgn | 28 |
| pgn | 28 |
| pgn | 29 |
| pgn | 29 |
| pgn | 29 |
| pgn | 29 |
| pgn | 29 |
| pgn | 30 |
| pgn | 30 |
| pgn | 30 |
| pgn | 30 |
| pgn | 30 |
| pgn | 30 |
| pgn | 31 |
| pgn | 31 |
| pgn | 31 |
| pgn | 31 |
| pgn | 31 |
| pgn | 32 |
| pgn | 32 |
| pgn | 32 |
| pgn | 32 |
| pgn | 32 |
| pgn | 33 |
| pgn | 33 |
| pgn | 33 |
| pgn | 33 |
| pgn | 33 |
| pgn | 33 |
| pgn | 33 |
| pgn | 34 |
| pgn | 34 |
| pgn | 34 |
| pgn | 34 |
| pgn | 34 |
| pgn | 35 |
| pgn | 35 |
| pgn | 35 |
| pgn | 35 |
| pgn | 35 |
| pgn | 35 |
| pgn | 36 |
| pgn | 36 |
| pgn | 36 |
| pgn | 36 |
| pgn | 36 |
| pgn | 37 |
| pgn | 37 |
| pgn | 37 |
| pgn | 37 |
| pgn | 37 |
| pgn | 38 |
| pgn | 38 |
| pgn | 38 |
| pgn | 38 |
| pgn | 39 |
| pgn | 39 |
| pgn | 39 |
| pgn | 39 |
| pgn | 39 |
| pgn | 40 |
| pgn | 40 |
| pgn | 40 |
| pgn | 40 |
| pgn | 40 |
| pgn | 41 |
| pgn | 41 |
| pgn | 41 |
| pgn | 41 |
| pgn | 41 |
| pgn | 42 |
| pgn | 42 |
| pgn | 42 |
| pgn | 42 |
| pgn | 42 |
| pgn | 42 |
| pgn | 43 |
| pgn | 43 |
| pgn | 43 |
| pgn | 43 |
| pgn | 43 |
| pgn | 44 |
| pgn | 44 |
| pgn | 44 |
| pgn | 44 |
| pgn | 44 |
| pgn | 52 |
| pgn | 74 |
| pgn | 81 |
| pgn | 82 |
| pgn | 90 |
| pgn | 98 |
| pgn | _0 |
| pgn | _0 |
| pgn | _0 |
| pgn | _0 |
| pgn | _0 |
| pgn | _0 |
| pgn | a2 |
| pgn | a2 |
| pgn | a2 |
| pjn | 11 |
| pjn | 11 |
| pjn | 11 |
| pjn | 11 |
| pjn | 11 |
| pjn | 12 |
| pjn | 12 |
| pjn | 12 |
| pjn | 12 |
| pjn | 13 |
| pjn | 13 |
| pmg | 22 |
| png | 10 |
| png | 10 |
| png | 12 |
| png | 13 |
| png | 13 |
| png | 14 |
| png | 14 |
| png | 14 |
| png | 14 |
| png | 14 |
| png | 14 |
| png | 15 |
| png | 15 |
| png | 15 |
| png | 15 |
| png | 15 |
| png | 16 |
| png | 16 |
| png | 16 |
| png | 16 |
| png | 17 |
| png | 18 |
| png | 22 |
| png | 24 |
| png | 26 |
| png | 30 |
| png | 34 |
| png | 38 |
| png | 39 |
| png | 41 |
| png | 52 |
| png | 54 |
| png | 56 |
| png | 58 |
| png | 60 |
| png | 62 |
| png | 64 |
| png | 68 |
| png | 70 |
| png | 72 |
| png | 74 |
| png | 76 |
| png | 78 |
| png | 80 |
| png | 82 |
| png | 84 |
| png | 90 |
| png | 92 |
| png | 94 |
| png | 96 |
| png | 98 |
| pnj | 10 |

Vamos a ver cuantos grupos de patrones hay y cuantos archivos hay de cada grupo.

#+begin_src awk :dir ./Libro :tangle agrupar.awk
{
cabeza=substr($0,1,3);
grupos[cabeza] += 1;
extra=substr($0,4,2);
if (!match(extra,/^[[:digit:]]+$/)){
gextra[extra] += 1;}
}
END{
print "Encabezados"
for (grupo in grupos){
print grupo, grupos[grupo];
}
print "Extras"
for (extra in gextra){
print extra, gextra[extra];
}
}
#+end_src

Vamos a *tanglear* -> Cu Cc Cv Ct

#+begin_src shell :dir ./Libro :results output
cat ../agrupar.awk
#+end_src

#+RESULTS:
#+begin_example
{
cabeza=substr($0,1,3);
grupos[cabeza] += 1;
extra=substr($0,4,2);
if (!match(extra,/^[[:digit:]]+$/)){
gextra[extra] += 1;}
}
END{
print "Encabezados"
for (grupo in grupos){
print grupo, grupos[grupo];
}
print "Extras"
for (extra in gextra){
print extra, gextra[extra];
}
}
#+end_example


#+name: grupos_patrones
#+begin_src shell :dir ./Libro :results output
ls *.pnm | awk -f ../agrupar.awk
#+end_src

#+RESULTS: grupos_patrones
#+begin_example
Encabezados
pan 2
ong 1
pmg 1
pgm 1
png 51
pgn 172
PNJ 1
pnj 1
pjn 11
pg2 1
Extras
_0 6
a2 3
g1 1
g2 1
#+end_example


Esta tabla de grupos y patrones de archivos [[grupos_patrones]] nos indica los casos mínimos que deberíamos considerar.

¿A qué queremos llegar?

#+begin_example
pgn_NROIZQ-NRODER.pnm
#+end_example

Hay un comando 

- mv para cambiar de nombre
- en awk hay una función *system*

Vamos a crear el directorio *corregidos*.

#+begin_src shell :dir ./Libro
mkdir corregidos
#+end_src

#+begin_src awk :dir ./Libro :tangle correcciones.awk
{
cabeza=substr($0,0,3);
extra=substr($0,4,2);
# separacion del nombre y extension
split($0,nombre_ext,".");# ["nombre", "ext"]
nombre=nombre_ext[1];
largo=length(nombre);
# caso de pg2, en cabeza un numero
if (cabeza=="pg2"){
numeros=substr(nombre, 3, largo);
}
else{
if (match(extra, /_0/)  || match(extra, /[[:alpha:]][[:digit:]]/)){
 numeros=substr(nombre, 5, largo);
} else{
 numeros=substr(nombre, 4, largo);
}
split(numeros,IZQ_DER,"-");
izq=sprintf("%03d",IZQ_DER[1]);
der=sprintf("%03d",IZQ_DER[2]);
}
comando="mv "$0" corregidos/pgn_"izq"-"der".pnm"
print comando
#printf "%03d-%03d\n",izq, der
system(comando);
}
#+end_src

Ejecutamos el script sobre la lista de archivos.

#+begin_src shell :dir ./Libro
ls  *.pnm|grep "b\.pnm"
#+end_src

#+RESULTS:
: pgn136-137b.pnm

Cambiamos el nombre de esta archivo en especial.
#+begin_src shell :dir ./Libro
mv pgn136-137b.pnm pgn_136-137.pnm
#+end_src


Acá, luego de cambiar el nombre del archivo, no debe listar nada.

#+begin_src shell :dir ./Libro
ls  *.pnm|grep "b\.pnm"
#+end_src


#+RESULTS:


#+begin_src shell :dir ./Libro
mkdir corregidos
#+end_src

#+RESULTS:


#+begin_src shell :dir ./Libro :results output
ls *.pnm | awk -f ../correcciones.awk
#+end_src


#+begin_src shell :dir ./Libro
ls corregidos
#+end_src

#+RESULTS:
| pgn_000-001.pnm |
| pgn_000-137.pnm |
| pgn_002-003.pnm |
| pgn_004-005.pnm |
| pgn_006-007.pnm |
| pgn_008-009.pnm |
| pgn_010-011.pnm |
| pgn_012-013.pnm |
| pgn_014-015.pnm |
| pgn_016-017.pnm |
| pgn_018-019.pnm |
| pgn_022-023.pnm |
| pgn_024-025.pnm |
| pgn_026-027.pnm |
| pgn_028-029.pnm |
| pgn_030-031.pnm |
| pgn_033-034.pnm |
| pgn_035-036.pnm |
| pgn_039-040.pnm |
| pgn_041-042.pnm |
| pgn_042-043.pnm |
| pgn_052-053.pnm |
| pgn_054-055.pnm |
| pgn_056-057.pnm |
| pgn_058-059.pnm |
| pgn_060-061.pnm |
| pgn_062-063.pnm |
| pgn_064-065.pnm |
| pgn_068-069.pnm |
| pgn_070-071.pnm |
| pgn_072-073.pnm |
| pgn_074-075.pnm |
| pgn_076-077.pnm |
| pgn_078-079.pnm |
| pgn_080-081.pnm |
| pgn_081-082.pnm |
| pgn_082-083.pnm |
| pgn_084-085.pnm |
| pgn_090-091.pnm |
| pgn_092-093.pnm |
| pgn_094-095.pnm |
| pgn_096-097.pnm |
| pgn_098-099.pnm |
| pgn_100-101.pnm |
| pgn_102-103.pnm |
| pgn_106-107.pnm |
| pgn_108-109.pnm |
| pgn_110-111.pnm |
| pgn_112-113.pnm |
| pgn_114-115.pnm |
| pgn_116-117.pnm |
| pgn_118-119.pnm |
| pgn_120-121.pnm |
| pgn_124-125.pnm |
| pgn_126-127.pnm |
| pgn_128-129.pnm |
| pgn_130-131.pnm |
| pgn_132-133.pnm |
| pgn_134-135.pnm |
| pgn_136-137.pnm |
| pgn_138-139.pnm |
| pgn_140-141.pnm |
| pgn_142-143.pnm |
| pgn_144-145.pnm |
| pgn_146-147.pnm |
| pgn_148-149.pnm |
| pgn_150-151.pnm |
| pgn_152-153.pnm |
| pgn_154-155.pnm |
| pgn_156-157.pnm |
| pgn_158-159.pnm |
| pgn_160-161.pnm |
| pgn_162-163.pnm |
| pgn_164-165.pnm |
| pgn_166-167.pnm |
| pgn_168-169.pnm |
| pgn_170-171.pnm |
| pgn_172-173.pnm |
| pgn_174-000.pnm |
| pgn_176-177.pnm |
| pgn_178-179.pnm |
| pgn_180-181.pnm |
| pgn_182-183.pnm |
| pgn_184-185.pnm |
| pgn_186-187.pnm |
| pgn_188-189.pnm |
| pgn_190-191.pnm |
| pgn_192-193.pnm |
| pgn_194-195.pnm |
| pgn_196-197.pnm |
| pgn_198-199.pnm |
| pgn_200-201.pnm |
| pgn_202-203.pnm |
| pgn_204-204.pnm |
| pgn_204-205.pnm |
| pgn_206-207.pnm |
| pgn_208-209.pnm |
| pgn_210-211.pnm |
| pgn_212-213.pnm |
| pgn_216-217.pnm |
| pgn_218-219.pnm |
| pgn_220-221.pnm |
| pgn_222-223.pnm |
| pgn_224-225.pnm |
| pgn_226-227.pnm |
| pgn_228-229.pnm |
| pgn_230-231.pnm |
| pgn_232-233.pnm |
| pgn_234-235.pnm |
| pgn_236-237.pnm |
| pgn_238-239.pnm |
| pgn_240-241.pnm |
| pgn_242-243.pnm |
| pgn_244-245.pnm |
| pgn_246-247.pnm |
| pgn_248-249.pnm |
| pgn_250-251.pnm |
| pgn_252-253.pnm |
| pgn_254-255.pnm |
| pgn_256-257.pnm |
| pgn_258-259.pnm |
| pgn_260-261.pnm |
| pgn_262-263.pnm |
| pgn_264-265.pnm |
| pgn_266-267.pnm |
| pgn_268-269.pnm |
| pgn_270-271.pnm |
| pgn_272-273.pnm |
| pgn_274-275.pnm |
| pgn_276-277.pnm |
| pgn_278-279.pnm |
| pgn_280-281.pnm |
| pgn_282-283.pnm |
| pgn_284-285.pnm |
| pgn_286-287.pnm |
| pgn_288-289.pnm |
| pgn_290-291.pnm |
| pgn_292-293.pnm |
| pgn_294-295.pnm |
| pgn_296-297.pnm |
| pgn_298-299.pnm |
| pgn_300-301.pnm |
| pgn_302-303.pnm |
| pgn_304-305.pnm |
| pgn_306-307.pnm |
| pgn_308-309.pnm |
| pgn_310-311.pnm |
| pgn_312-313.pnm |
| pgn_314-315.pnm |
| pgn_316-317.pnm |
| pgn_318-319.pnm |
| pgn_320-321.pnm |
| pgn_322-323.pnm |
| pgn_324-325.pnm |
| pgn_326-327.pnm |
| pgn_328-329.pnm |
| pgn_330-331.pnm |
| pgn_332-333.pnm |
| pgn_334-000.pnm |
| pgn_334-335.pnm |
| pgn_336-337.pnm |
| pgn_338-339.pnm |
| pgn_340-341.pnm |
| pgn_342-343.pnm |
| pgn_344-345.pnm |
| pgn_346-347.pnm |
| pgn_348-349.pnm |
| pgn_350-351.pnm |
| pgn_352-353.pnm |
| pgn_354-355.pnm |
| pgn_356-357.pnm |
| pgn_358-359.pnm |
| pgn_360-361.pnm |
| pgn_362-363.pnm |
| pgn_364-365.pnm |
| pgn_366-367.pnm |
| pgn_368-369.pnm |
| pgn_370-371.pnm |
| pgn_372-373.pnm |
| pgn_374-375.pnm |
| pgn_376-377.pnm |
| pgn_378-379.pnm |
| pgn_380-381.pnm |
| pgn_384-385.pnm |
| pgn_385-386.pnm |
| pgn_386-387.pnm |
| pgn_388-389.pnm |
| pgn_390-391.pnm |
| pgn_392-393.pnm |
| pgn_394-395.pnm |
| pgn_396-397.pnm |
| pgn_398-399.pnm |
| pgn_400-401.pnm |
| pgn_402-403.pnm |
| pgn_404-405.pnm |
| pgn_406-407.pnm |
| pgn_408-409.pnm |
| pgn_410-411.pnm |
| pgn_412-413.pnm |
| pgn_414-415.pnm |
| pgn_416-417.pnm |
| pgn_418-419.pnm |
| pgn_420-421.pnm |
| pgn_422-423.pnm |
| pgn_424-425.pnm |
| pgn_426-427.pnm |
| pgn_428-429.pnm |
| pgn_430-431.pnm |
| pgn_432-433.pnm |
| pgn_434-435.pnm |
| pgn_436-437.pnm |
| pgn_438-439.pnm |
| pgn_440-441.pnm |
| pgn_442-443.pnm |
| pgn_444-445.pnm |
| pgn_446-447.pnm |
| pgn_448-449.pnm |


[[file:./Libro/corregidos/pgn_448-449.pnm]]


Vamos a crear un comando para rotar.

** Convertir a png

#+begin_src shell :dir ./Libro/corregidos
mkdir en_png
#+end_src

#+RESULTS:

#+begin_src shell :dir ./Libro/corregidos :results output
ls -F |grep "/$"
#+end_src

#+RESULTS:
: en_png/

#+begin_src shell :dir ./Libro/corregidos :results output
ls *.pnm | awk -F'.' '{com="convert -rotate -90 "$0" "$1".png"; print com}' | bash
#+end_src


Movemos los png a en_png

#+begin_src shell :dir ./Libro/corregidos
mv *.png en_png
#+end_src

#+RESULTS:

** Unimos todas las imágenes en un pdf.

Habilitando las políticas de seguridad de pdf, en la configuración de imagemagick, se podrá
manipular el grupo de imágenes para generar un libro en pdf.

#+begin_src shell :dir ./Libro/corregidos/en_png
img2pdf *.png --output libro.pdf
#+end_src

#+RESULTS:

