int main() {
/* print Fahrenheit-Celsius table for fahr=0...300 */
float fahr, celsius;
float lower, upper, step;
lower = 0;
upper = 300;
step = 20;
fahr = lower;
while(fahr <= upper){
  celsius = (5./9.0)*(fahr-32);
  printf("%5.1f\t%8.3f\n", fahr, celsius);
  fahr = fahr + step;
}
return 0;
}
