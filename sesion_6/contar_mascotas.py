mascotas=[["David", 33, "perro"], ["Juan", 24, "gato"], ["Daniela", 56, "perro"], ["Carla", 33, "gallina"], ["Pedro", 22, "hurón"], ["Esteban", 14, "perro"], ["Marcela", 88, "gato"], ["Nicolás", 12, "gato"], ["Paula", 67, "gallina"], ["Elda", 66, "pato"], ["Daniel", 55, "perro"], ["Tom", 45, "gato"]]
from pprint import pprint
pprint(mascotas)
print(type(mascotas))

llaves = ["nombre","edad","mascota"]
lista = list(map(lambda fila:dict(zip(llaves, fila)),mascotas))
pprint(lista)

lista.sort(key=lambda fila: fila.get("mascota"))

from itertools import groupby


for group, values in groupby(lista, key=lambda e: e.get("mascota")):
    print(group, len([e for e in values]))
